
const FIRST_NAME = "Alexandra Ioana";
const LAST_NAME = "Maftei";
const GRUPA = "1092";

/**
 * Make the implementation here
 */
class Employee {
    constructor(name, surname, salary)
    {
        this.name=name;
        this.surname=surname;
        this.salary=salary;
    }

    getDetails()
    {
        return this.name + " " + this.surname + " " + this.salary;
    }
}

class SoftwareEngineer extends Employee {
   constructor(name, surname, salary, experience)
   {
       super(name, surname, salary);
       if (experience !== undefined)
        {
            this.experience=experience;
        }
        else
        {
            this.experience="JUNIOR";
        }
   }

   applyBonus(experience)
   {
        if (this.experience.toUpperCase() == "MIDDLE")
       {
           return 1.15 * this.salary;
       }
       else if (this.experience.toUpperCase() == "SENIOR")
       {
           return 1.2 * this.salary;
       }
       else 
       {
           return 1.1 * this.salary;
        }
   }
}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    Employee,
    SoftwareEngineer
}

